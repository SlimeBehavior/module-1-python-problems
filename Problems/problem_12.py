# This function takes a list of lists and returns the longest inner list.

# Note: The input data will have only one longest list.

# Here are some examples:

#          lists	           output
#           []	                None
#          [[1]]	            [1]
#   [[1], [1,2,3], [1,2]]	  [1,2,3]

# Answer:

#   def find_longest(lists):
#       longest = None               
#       max_length = 0               
#
#       for list in lists:       
#           length = len(list)     
#           if length > max_length:  
#               max_length = length   
#               longest = list      
#       return longest     