# This function count_entries will take a string that's composed of values that are separated by some "delimiter" character, like a comma. It then returns the number of values in that string that are separated by the provided delimiter.

# Here are some example inputs and outputs:

#       Input	Separator	Output
#        ""	       any	      0
#        "a"	   ","	      1
#        ","	   ","	      2
#      "a,b,c"	   ","	      3
#      "a,b,c"	   ":"	      1

# Please complete the count_entries function here.

# Function:

#   def count_entries(line, separator):
#       your code here
        