# This function converts temperature measured in degrees Fahrenheit (°F) to degrees Celsius (°C). Here are three examples:
# Chart:
#         °F	°C
#         32	 0
#         61	16.11111111
#         212	100

# The formula is:
#   °C = (°F - 32) * 5/9

# Answer:

#   def f_to_c(fahrenheit):
#       return (fahrenheit - 32) * 5/9