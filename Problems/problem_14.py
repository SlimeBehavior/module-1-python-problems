# In credit cards, network messages, and other kinds of data, there's usually an extra piece of information called a checksum. The checksum allows you to quickly check to see if the data is valid.

# Implement the checksum method using the following algorithm:

#   * Start with a sum of zero
#   * For each letter in the message:
#       * Convert the letter to a number using the ord function
#       * Add the number to the sum
#   * Use modulo division % to find the remainder when the sum is divided by 26
#   * Add 97 to the remainder
#   * Convert the sum from the previous step to a letter using the chr function
#   * Return that letter

# Here are some examples of inputs and outputs:

#       Input	       Output
#       "bat"	        "z"
#       "cat"	        "a"
#   "hack reactor"	    "v"

# Using the modulo division operator (%) lets you find the remainder of a division calculation.

# 260 % 26  # = 0
# 261 % 26  # = 1
# 270 % 26  # = 10

# Function:

#   def checksum(message):
#       your code here
