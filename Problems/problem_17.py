# The function below takes a list of items and returns a copy of the list with all of the duplicate items removed.

# Examples:

#   input	output
#    []	      []
#   [1,2]	 [1,2]
#  [1,1,2]	 [1,2]
#  [1,1,1]	 [1]

# Function:

#   def unique_elements(items):
#       your code here