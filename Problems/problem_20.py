# The following function is simple enough. It formats a person's name from the fields in a dictionary. While the middle field is supposed to always be present, sometimes it's omitted and the function fails.

# Here are a couple samples of how the program should run:

# name = {"first": "Jocelyn", "middle": "Maria", "last": "Payne"}
# print(format_name(name))  # --> "Jocelyn M. Payne"

# name = {"first": "Jocelyn", "middle": "", "last": "Payne"}
# print(format_name(name))  # --> "Jocelyn Payne"

# name = {"first": "Jocelyn", "last": "Payne"}
# print(format_name(name))  # --> "Jocelyn Payne"

# Please fix the function below so that it will pass its unit tests.

# Function:

#   def format_name(name):
#       return name["first"] + " " + name[""][0] + ". " + name["last"]