# This function takes two values: numerator and denominator. If the quotient (the numerator divided by the denominator) is an integer, then this function returns True. Otherwise, it returns False.

# Here are some examples:

#       numerator	denominator    output
#           6	        1.5	        True
#           6	        2	        True
#           6	        3	        True
#           6	        4	        False

# You can test if a number is an integer by using the int function.
# Example:

#   if int(x) == x:    # This is true if x is an integer
#       print("I am integer!")

# If the denominator is zero, it should return False.

# If the numerator is zero, it should return True.

# Function:

#   def divides_evenly(numerator, denominator):
#       your code here