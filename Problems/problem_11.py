# This function takes a list of rectangles described by their heights and widths. It sums up the areas of all of the rectangles.

# Here's an example input for the sum_areas function with one rectangle description in it:

# rectangles = [
#     {
#         "height": 10,
#         "width": 8.5,
#     },
# ]
# The total area for the example above would be 10 x 8.5 = 85.

# Here's an example with two items:

# rectangles = [
#     { "height": 11, "width": 9,   },
#     { "height": 5,  "width": 1.5, },
# ]

# The total area for the example above is: (11 x 9) + (5 x 1.5) = 106.5.

# If the list is empty, then the total area should be 0.

# Function:

#   def sum_areas(rectangles):
#       your code here