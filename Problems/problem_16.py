# The join_numbers function takes a list of single-digit numbers and builds a string that contains all of the digits in order.

#  Input	  Output
#   []	        ""
#   [1]	        "1"
#   [1, 2]	    "12"
#   [3, 2, 1]	"321"

# Function: 

#   def join_numbers(digits):
#       your code here