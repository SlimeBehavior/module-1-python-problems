# Implement the function multiply_all(), which returns the product of all of its arguments.

# Sample Inputs/Outputs:
  
#     numbers   output
#     (none)	   1
#       3	       3
#   1, 2, 3, 4    24

# Answer:

#    def multiply_all(*numbers):
#        total = 1
#        for x in numbers:
#            total *= x  
#        return total  