# Implement the function calculate_total_with_tax, which will return the total of the bill given the amount and the tax rate.

#     amount	tax_rate	output
#       10	      0.1	      11
#       10	      0.15	      11.5
#       30	      0.2	      36

# To calculate the total, you need to take the original amount and add it to the product of the amount and the tax rate.

# Function:

#   def calculate_total_with_tax(amount, tax_rate):
#       your code here